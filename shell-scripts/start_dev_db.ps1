docker stop some-postgres
docker rm some-postgres
docker run --name some-postgres -e POSTGRES_DB=dev -e POSTGRES_USER=root -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 -d postgres