package dev.kotylu.db;

import dev.kotylu.configuration.AddonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TableNameFormatter {

    private final AddonConfig addonConfig;

    @Autowired
    public TableNameFormatter(AddonConfig config) {
        this.addonConfig = config;
    }

    public String getNameWithSchema(String name) {
        return String.format("%s.%s", addonConfig.getKey(), name);
    }
}
