FROM debian:10 as builder

# system setup
RUN apt-get update && apt-get upgrade -y
RUN apt-get install curl wget -y

# JAVA installation
RUN apt-get install openjdk-11-jdk -y
#ENV JAVA_HOME $(update-alternatives --list java | sed -E 's/\/bin\/java//g')

# MAVEN installation
RUN mkdir -p /maven/extract
WORKDIR /maven
RUN wget https://dlcdn.apache.org/maven/maven-3/3.9.3/binaries/apache-maven-3.9.3-bin.tar.gz
RUN local=$(sha512sum apache-maven-3.9.3-bin.tar.gz | tr " "  "\n" | sed -n 1,1p); \
    remote=$(curl https://downloads.apache.org/maven/maven-3/3.9.3/binaries/apache-maven-3.9.3-bin.tar.gz.sha512); \
    if [ $local != $remote ]; \
    then exit 1; \
    fi
RUN tar xzvf $(ls apache-maven-*.tar.gz) -C extract --strip-components=1
ENV PATH /maven/extract/bin:$PATH

# build setup
RUN mkdir /app-build
WORKDIR /app-build
COPY . /app-build

# run build
RUN mvn clean package



FROM debian:10 as runner

RUN apt-get update && apt-get upgrade -y
RUN apt-get install openjdk-11-jre -y

RUN mkdir /app
WORKDIR /app
COPY --from=builder /app-build/target ./

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app/sample.jar"]

