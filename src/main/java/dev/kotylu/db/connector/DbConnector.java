package dev.kotylu.db.connector;

import dev.kotylu.configuration.DataSourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.*;
import java.sql.*;

@Component("db-con")
public class DbConnector {

    private final Logger log = LoggerFactory.getLogger(DbConnector.class);
    private DataSourceConfig dsConfig;

    @Autowired
    public DbConnector(DataSourceConfig dsConfig) {
        this.dsConfig = dsConfig;
    }

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(dsConfig.getUrl(), dsConfig.getUsername(), dsConfig.getPassword());
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
