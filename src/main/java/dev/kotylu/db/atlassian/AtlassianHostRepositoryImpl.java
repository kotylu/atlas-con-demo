package dev.kotylu.db.atlassian;

import com.atlassian.connect.spring.AddonAuthenticationType;
import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import dev.kotylu.db.TableNameFormatter;
import dev.kotylu.db.connector.DbConnector;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.constraints.NotNull;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import java.sql.Date;

public class AtlassianHostRepositoryImpl implements AtlassianHostRepository {

    private final Logger log = LoggerFactory.getLogger(AtlassianHostRepositoryImpl.class);
    @Autowired
    private TableNameFormatter tableNameFormatter;
    @Autowired
    private DbConnector db;

    private String getTableName() {
        return tableNameFormatter.getNameWithSchema("atlassian_host");
    }

    private String getQueryWithTableName(String query) {
        String res = String.format(query, getTableName());
        log.warn("SQL TO EXECUTE: {}", res);
        return res;
    }

    @Override
    public Optional<AtlassianHost> findFirstByBaseUrlOrderByLastModifiedDateDesc(String base_url) {
        String query = getQueryWithTableName("SELECT client_key, shared_secret, base_url, product_type, description, service_entitlement_number, addon_installed, created_date, last_modified_date, created_by, last_modified_by, oauth_client_id, cloud_id, authentication "
                + " FROM %s "
                + " where base_url = ? "
                + " order by last_modified_date desc");
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, base_url);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                return Optional.of(makeAtlassianHostFromRS(rs));

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public <S extends AtlassianHost> S save(S entity) {
        if (existsById(entity.getClientKey())) {
            Optional<AtlassianHost> updated = updateExistingAtlassianHost(entity);
            if (updated.isPresent())
                return entity;
            else
                return null;
        }
        else {
            Optional<AtlassianHost> inserted = insertNewAtlassianHost(entity);
            if (inserted.isPresent())
                return entity;
            else
                return null;
        }
    }

    private Optional<AtlassianHost> updateExistingAtlassianHost(AtlassianHost existingHost) {
        String query = getQueryWithTableName("update %s set "
                + " client_key = ?, "
                + " oauth_client_id = ?, "
                + " shared_secret = ?, "
                + " authentication = ?, "
                + " cloud_id = ?, "
                + " base_url = ?, "
                + " product_type = ?, "
                + " description = ?, "
                + " service_entitlement_number = ?, "
                + " addon_installed = ?, "
                + " created_by = ?, "
                + " last_modified_by = ? "
                + " where client_key = ? ");
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            injectAtlassianHostToPrepStatement(ps, existingHost);
            ps.setString(13, existingHost.getClientKey());
            ps.executeUpdate();
            return Optional.of(existingHost);
        }
        catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Optional.empty();
    }

    private Optional<AtlassianHost> insertNewAtlassianHost(AtlassianHost newHost) {
        String query = getQueryWithTableName("insert into %s ( "
                + " client_key, oauth_client_id, shared_secret, authentication, cloud_id, base_url, product_type,"
                + " description, service_entitlement_number, addon_installed, "
                + " created_by, last_modified_by "
                + " ) values ( "
                + " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? "
                + " )");
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            injectAtlassianHostToPrepStatement(ps, newHost);
            ps.executeUpdate();
            return Optional.of(newHost);
        }
        catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Optional.empty();
    }

    @NotNull
    @Override
    public <S extends AtlassianHost> Iterable<S> saveAll(Iterable<S> entities) {
        for (S entity : entities) {
            this.save(entity);
        }
        return entities;
    }

    @Override
    public Optional<AtlassianHost> findById(String s) {
        String query = getQueryWithTableName("select * from %s where client_key = ?");
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, s);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                AtlassianHost host = makeAtlassianHostFromRS(rs);
                return Optional.of(host);
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        log.error("have no found host with id: {}", s);
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        String query = getQueryWithTableName("select count(*) as count from %s where client_key = ?");
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, s);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                return rs.getInt("count") > 0;
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    @Override
    public Iterable<AtlassianHost> findAll() {
        List<AtlassianHost> result = new ArrayList<>();
        String query = getQueryWithTableName("select * from %s");
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                result.add(this.makeAtlassianHostFromRS(rs));
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return result;
    }

    @Override
    public Iterable<AtlassianHost> findAllById(Iterable<String> strings) {
        List<AtlassianHost> result = new ArrayList<>();
        for (String id : strings) {
            Optional<AtlassianHost> optionalHost = this.findById(id);
            optionalHost.ifPresent(result::add);
        }
        return result;
    }

    @Override
    public long count() {
        String query = getQueryWithTableName("select count(*) as count from %s");
        try (Connection con = this.db.getConnection()) {
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                return rs.getInt("count");
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return 0;
    }

    @Override
    public void deleteById(String s) {
        log.error("not implemented delbyid");
    }

    @Override
    public void delete(AtlassianHost entity) {

        log.error("not implemented del");
    }

    @Override
    public void deleteAllById(Iterable<? extends String> strings) {

        log.error("not implemented delallbyid");
    }

    @Override
    public void deleteAll(Iterable<? extends AtlassianHost> entities) {

        log.error("not implemented delallbyenti");
    }

    @Override
    public void deleteAll() {

        log.error("not implemented delall");
    }

    private AtlassianHost makeAtlassianHostFromRS(ResultSet rs) throws SQLException {
        AtlassianHost host = new AtlassianHost();
        host.setClientKey(rs.getString("client_key"));
        host.setOauthClientId(rs.getString("oauth_client_id"));
        host.setSharedSecret(rs.getString("shared_secret"));
        host.setAuthentication(AddonAuthenticationType.valueOf(rs.getString("authentication")));
        host.setCloudId(rs.getString("cloud_id"));
        host.setBaseUrl(rs.getString("base_url"));
        host.setProductType(rs.getString("product_type"));
        host.setDescription(rs.getString("description"));
        host.setServiceEntitlementNumber(rs.getString("service_entitlement_number"));
        host.setAddonInstalled(rs.getBoolean("addon_installed"));
        host.setCreatedDate(makeCalendarFromSqlDate(rs.getDate("created_date")));
        host.setLastModifiedDate(makeCalendarFromSqlDate(rs.getDate("last_modified_date")));
        host.setCreatedBy(rs.getString("created_by"));
        host.setLastModifiedBy(rs.getString("last_modified_by"));
        return host;
    }

    private void injectAtlassianHostToPrepStatement(PreparedStatement ps, AtlassianHost host) throws SQLException {
        ps.setString(1, host.getClientKey());
        ps.setString(2, host.getOauthClientId());
        ps.setString(3, host.getSharedSecret());
        ps.setString(4, host.getAuthentication().toString());
        ps.setString(5, host.getCloudId());
        ps.setString(6, host.getBaseUrl());
        ps.setString(7, host.getProductType());
        ps.setString(8, host.getDescription());
        ps.setString(9, host.getServiceEntitlementNumber());
        ps.setBoolean(10, host.isAddonInstalled());
        // created_date is automatically created by postgresql
        //ps.setDate(11, new Date(host.getCreatedDate().getTime().getTime()));
        // last_modified_date is automatically updated by postgresql
        //ps.setDate(12, new Date(host.getLastModifiedDate().getTime().getTime()));
        ps.setString(11, host.getCreatedBy());
        ps.setString(12, host.getLastModifiedBy());
    }

    private Calendar makeCalendarFromSqlDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

}
