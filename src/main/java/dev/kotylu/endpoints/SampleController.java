package dev.kotylu.endpoints;

import com.atlassian.connect.spring.AtlassianHostUser;
import dev.kotylu.configuration.YAMLConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SampleController {
    private final Logger log = LoggerFactory.getLogger(SampleController.class);

    @Autowired
    private YAMLConfig config;

    @RequestMapping(value = "/basic-connect", method = RequestMethod.GET)
    @ResponseBody
    public String home(@AuthenticationPrincipal AtlassianHostUser user) {
        return "Hello user "+user.getUserAccountId();
    }

    @RequestMapping(value = "/basic", method = RequestMethod.GET)
    @ResponseBody
    public String home() {
        log.warn("accessed /basic endpoint");
        return " <!DOCTYPE html> <html lang='en'> <head> <link rel='stylesheet' href='https://unpkg.com/@atlaskit/css-reset@2.0.0/dist/bundle.css' media='all'> <script src='https://connect-cdn.atl-paas.net/all.js' async></script> </head> <body> <section id='content' class='ac-content'> <h1>Hello World</h1> </section> </body> </html> ";
    }

    @RequestMapping(value = "/config", method = RequestMethod.GET)
    @ResponseBody
    public String config() {
        log.warn("accessed /config endpoint");
        return " <!DOCTYPE html> <html lang='en'> <head> <link rel='stylesheet' href='https://unpkg.com/@atlaskit/css-reset@2.0.0/dist/bundle.css' media='all'> <script src='https://connect-cdn.atl-paas.net/all.js' async></script> </head> <body> <section id='content' class='ac-content'>"
                + "<h1> "+ config.pretty() +" </h1>"
                + "</section> </body> </html> ";
    }

    @RequestMapping(value = "/custom-installed", method = RequestMethod.GET)
    @ResponseBody
    public String installed() {
        return "[]";
    }
    @RequestMapping(value = "/custom-uninstalled", method = RequestMethod.GET)
    @ResponseBody
    public String uninstalled() {
        return "[]";
    }


    /*
    @RequestMapping(value = "/template", method = RequestMethod.GET)
    public ModelAndView template(@RequestParam String username) {
        ModelAndView model = new ModelAndView();
        model.setViewName("template");
        model.addObject("username", username);
        return model;
    }
    */
}
