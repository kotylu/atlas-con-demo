package dev.kotylu.db.atlassian;

import com.atlassian.connect.spring.AtlassianHostRepository;
import dev.kotylu.db.connector.DbConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

@Configuration
@EnableJdbcRepositories(basePackages = {
        "dev.kotylu.db.atlassian"
})
public class AtlassianHostRepositoryConfig {


    @Bean
    AtlassianHostRepository atlassianHostRepositoryFactory() {
        System.out.println("accessing custom atlas host repo");
        return new AtlassianHostRepositoryImpl();

    }
}
