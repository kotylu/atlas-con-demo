COLUMNS:
    clientKey,
    oauthClientId,
    sharedSecret,
    authentication,
    cloudId,
    baseUrl,
    productType,
    description,
    serviceEntitlementNumber,
    addonInstalled,
    createdDate,
    lastModifiedDate,
    createdBy,
    lastModifiedBy





create schema IF NOT EXISTS sample;

DROP TABLE IF EXISTS sample.atlassian_host;

CREATE TABLE IF NOT EXISTS sample.atlassian_host (
	client_key varchar(255) NOT NULL,
	shared_secret varchar(255) NOT NULL,
	base_url varchar(255) NOT NULL,
	product_type varchar(255) NULL,
	description varchar(255) NULL,
	service_entitlement_number varchar(255) NULL,
	addon_installed bool NULL,
	created_date timestamp DEFAULT CURRENT_TIMESTAMP,
	last_modified_date timestamp DEFAULT CURRENT_TIMESTAMP,
	created_by varchar(255) NULL,
	last_modified_by varchar(255) NULL,
	oauth_client_id varchar(255) NULL,
	cloud_id varchar(255) NULL,
	authentication varchar(255) NULL,
	CONSTRAINT atlassian_host_pkey PRIMARY KEY (client_key)
);
CREATE INDEX IF NOT EXISTS idx_sample_atlassian_host_base_url ON sample.atlassian_host USING btree (base_url);

DROP FUNCTION IF EXISTS update_last_modified_date();
CREATE FUNCTION update_last_modified_date() RETURNS trigger AS $update_timestamp$
    BEGIN
        NEW.last_modified_date := current_timestamp;
        RETURN NEW;
    END;
$update_timestamp$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_last_modified_date ON sample.atlassian_host;
CREATE TRIGGER update_last_modified_date BEFORE INSERT OR UPDATE ON sample.atlassian_host
    FOR EACH ROW EXECUTE PROCEDURE update_last_modified_date();
